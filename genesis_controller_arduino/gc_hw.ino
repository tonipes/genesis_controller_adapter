#define inputCount 8
#define inputPinCount 6
#define selectPin 3

boolean inputPins[inputPinCount]= {2,4,6,5,7,8};
boolean inputState[inputCount]= {false};
boolean inputStateOld[inputCount]= {false};

void setup() {
  Serial.begin(9600);
  pinMode(13,OUTPUT);
  for(int i = 0; i < inputPinCount;i++){
    pinMode(inputState[i], INPUT);
  }
  pinMode(selectPin, OUTPUT);
}

void loop() {
  delay(16);
  for(int i = 0; i < inputCount;i++){
    inputStateOld[i] = inputState[i];
  }
  
  // select down ( btn a and start)
  digitalWrite(selectPin, LOW);
  boolean a = digitalRead(inputPins[0]);
  boolean start = digitalRead(inputPins[1]);
  inputState[6] = !a;
  inputState[7] = !start;
  
  
  // select up ( all else
  digitalWrite(selectPin, HIGH);
  for(int i = 0; i < inputPinCount;i++){
    boolean b = digitalRead(inputPins[i]);
    if(b){
      inputState[i] = false;
    }
    else{
      inputState[i] = true;
    }
  }
  
  for(int i = 0; i < inputCount;i++){
    if(inputState[i] != inputStateOld[i]){
      sendDataPackage(i);
    }
  }
}

// handles data package sending to serial port 
// sends package to serial bus. mode = 1 if button, 2 if analog and 0 if something else?
// this is newer and more robust version tailored to be used with sega controller (no analog)
void sendDataPackage(char i){
  
  // send button package. Data lenght is 2 bytes plus 2 bytes of header data
  // example package: 2151 (DataLenght: 2, Mode: 1, Input: 5, State: 1 (true))
    byte data[4];
    data[0] = 2;
    data[1] = 1;
    data[2] = i;
    data[3] = inputState[i];
    
    sendData(data, 2);
}

void sendData(byte data[], int dataLenght){ // make sure that data lenght is same as said in data
  dataLenght = dataLenght+2;
  
  for(int i = 0;i<dataLenght;i++){
      //Serial.write(data[i]);
      Serial.print(data[i]);
    }
    Serial.println("");
}
