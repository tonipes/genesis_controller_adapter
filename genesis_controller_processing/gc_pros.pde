import processing.serial.*;
import java.util.Map;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

Robot robot;
public static final int MAIN_DELAY = 16; // 16 => 60fps

boolean[] inputState = {false, false, false, false, false, false, false, false};

String[] buttonName = {"C",
                       "B",
                       "Left",
                       "Right",
                       "Down",
                       "Up",
                       "Start",
                       "A"};
                       
int[] keys =           {KeyEvent.VK_C, 
                        KeyEvent.VK_X,
                        KeyEvent.VK_LEFT,
                        KeyEvent.VK_RIGHT,
                        KeyEvent.VK_DOWN,
                        KeyEvent.VK_UP,  
                        KeyEvent.VK_ENTER,
                        KeyEvent.VK_Z};

Serial serialPort;

void setup() {
  size(400, 200); //make our canvas 200 x 200 pixels big
  //  initialize your serial port and set the baud rate to 9600
  println(Serial.list()[0]);
  serialPort = new Serial(this, Serial.list()[0], 9600);
  serialPort.bufferUntil('\n'); 
  
  try { 
  robot = new Robot();
  } 
  catch (AWTException e) {
    e.printStackTrace(); 
  }
  
}
void draw() {
  background(0);
  strokeWeight(10);
  stroke(255,255,255,255);
  textSize(20); 
  String inString = "";
  
  for(int i = 0; i < 8; i++){
    if(inputState[i])
      inString += "1";
    else
      inString += "0";
  }
  text("state: " + inString, 10,50);
}

void buttonAction(int i, boolean state){
  if(state){
    if(i >= 2 && i <= 4){
      //robot.keyPress(KeyEvent.VK_WINDOWS);
    }
    robot.keyPress(keys[i]);
    inputState[i] = true;
  }else if(!state){
    robot.keyRelease(keys[i]);
    if(i >= 2 && i <= 4){
      //robot.keyRelease(KeyEvent.VK_WINDOWS);
    }
    inputState[i] = false;
  }

}

void serialEvent(Serial myPort) {
 byte[] dataReading = myPort.readBytes();
 if(dataReading!=null){
    //inString = new String(dataReading);
    if(dataReading[0] == (byte)0x32){ // first byte check
      if(dataReading[1] == (byte)0x31){  //second byte check
        
        boolean state = (dataReading[3]!=(byte)0x30);
        int input = ((int)dataReading[2])-48;
        buttonAction(input, state);
      }
    }
 }
}
